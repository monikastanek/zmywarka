#include<iostream>
#include<cstdio>
#include<fstream>
#include<string>
#include <unistd.h>
#include<ctime>
using namespace std;
class cycle
{
    public:
        string name;
        int duration;
        int temp;
        int energy;



};

int info(char a)
{
    fstream file;
    string name,temp,energy;
    int duration;

    switch (a)
    {
        case 'n':file.open( "/Users/monikastanek/Desktop/zmywarka/normal.txt", ios::in);
        break;
        case 'e':file.open( "/Users/monikastanek/Desktop/zmywarka/eco.txt", ios::in);
        break;
        case 'i':file.open( "/Users/monikastanek/Desktop/zmywarka/intensive.txt", ios::in);
        break;
        case 'f':file.open( "/Users/monikastanek/Desktop/zmywarka/fast.txt", ios::in);
        break;
    }

  if( file.good() == true )
  {

      getline( file, name);
      file>>duration;
      file>>temp;
      file>>energy;
      file.close();
    }
    cout<<"cycle: "<<name<<endl<<"duration of a cycle: "<<duration<<" min"<<endl<<"water temperature: "<<temp<<"  C"<<endl<<"energy consumption: "<<energy<<" kWh"<<endl;

return duration;

}
char choose_cycle()
{
    char a='z';
    cout<<"please select cycle"<<endl;
    while(1)
    {

        cout<<"press: "<<endl<<"n for normal"<<endl<<"e for eco"<<endl<<"i for intensive"<<endl<<"f for fast"<<endl;
        cin>>a;
        system("clear");
        if(a=='n' || a=='e'|| a=='i' || a=='f')
        break;
        else
            cout<<"plese select one of the cycles"<<endl;

    }

    system("clear");
    return a;
}
int main()
{
    int a,b,c,d,e,f,g,h;
    time_t current_time, cycle_time, paused_time,paused_time1;
    c=0;

    while(1)
    {
        if(c==0)
        {
            cout<<"no cycles running now"<<endl;
            cout<<"To start cycle press 1"<<endl<<"To see information about cycle press 2"<<endl;
            cin>>a;
            system("clear");
            if(a==1)
            {
                f=choose_cycle();
                cout<<"cycle started"<<endl;
                c=1;
                usleep(2000000);
                system("clear");
                time(&current_time);
                g=info(f)*60;
                cycle_time=current_time+g;

            }
            else if(a==2)
            {
                b=choose_cycle();
                info(b);
                cout<<"to go back to main menu select any number"<<endl;
                cin>>a;
                system("clear");

            }
            else
            {
                cout<<"select number 1-2"<<endl;
            }
        }
        else if(c==1)
        {
            cout<<"currently running cycle:"<<endl;
            info(f);
            time(&current_time);
                h=(cycle_time-current_time)/60;
            if(current_time<cycle_time)
               cout<<endl<<"time remaning: "<<h<<" min"<<endl;
            else
            {
                cout<<"cycle finished"<<endl;
                c=0;
            }
            cout<<"To see information about different cycle press 1"<<endl<<"To pause/stop current cycle press 2"<<endl;
                    cin>>a;

            system("clear");
            if(a==1)
            {
                b=choose_cycle();
                info(b);
                cout<<"to go back to main menu select any number"<<endl;
                cin>>a;
                system("clear");


            }
            else if(a==2)
            {
                cout<<"currently running cycle:"<<endl;
                info(f);
                cout<<endl;
                cout<<"To pause current cycle press 1"<<endl<<"To stop current cycle press 2"<<endl;
                cin>>d;
                system("clear");
                if(d==1)
                {
                    c=2;
                    cout<<"cycle paused"<<endl;
                    usleep(2000000);
                    system("clear");
                    time(&paused_time);

                }
                if(d==2)
                {
                    c=0;
                    cout<<"cycle stopped"<<endl;
                    usleep(2000000);
                    system("clear");
                }
            }
            else
            {
                cout<<"select number 1-2"<<endl;
            }
        }
        else if(c==2)
        {
            paused_time1=time(&current_time)-paused_time;
            cout<<"currently running cycle:"<<endl;
            info(f);
            cout<<endl<<"cycle is paused "<<endl<<"to resume cycle press 1"<<endl<<"to quit cycle press 2"<<endl;
            cin>>e;
            system("clear");
            if(e==1)
            {
                c=1;
                cout<<"cycle resumed"<<endl;
                usleep(2000000);
                system("clear");

            }
            else if(e==2)
            {
                c=0;
                cout<<"cycle stopped"<<endl;
                usleep(2000000);
                system("clear");

            }
        }

    }


    return 0;
}

